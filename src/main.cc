#include <QtCore/qsystemdetection.h> // Q_OS_WINDOWS

#ifdef Q_OS_WINDOWS
#	include <QtPlugin>
	Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)
#endif

#include <QMessageBox>
#include <QApplication>

int main(int argc, char** argv)
{
	QApplication app(argc, argv);
	return QMessageBox::question(nullptr, "The title", "This is Qt6") == QMessageBox::Yes
		? 0
		: 1;
}
