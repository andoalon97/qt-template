import conans

class QtTestConan(conans.ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = (
        "qt/6.2.1"
    )

    options = {
        #"shared": [True, False], # No DLL support
    }

    default_options = {
        "qt:shared" : False,
        #"qt:with_sqlite3" : False,
        #"qt:with_harfbuzz" : False,
    }

